package com.dhara.mvpalbums.utils

import androidx.test.platform.app.InstrumentationRegistry
import java.io.BufferedReader
import java.io.InputStreamReader

class MockApiUtils {
    companion object {
        fun getPhotos(jsonPath: String): String {
            val buf = StringBuilder()

            val inputStream = InstrumentationRegistry.getInstrumentation().context.assets.open("api_mocks/$jsonPath")
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))

            var str: String? = bufferedReader.readLine()
            while (str != null) {
                buf.append(str)
                str = bufferedReader.readLine()
            }
            inputStream.close()
            bufferedReader.close()

            System.out.println("inside ApiUtils >> $jsonPath")
            return buf.toString()
        }
    }
}