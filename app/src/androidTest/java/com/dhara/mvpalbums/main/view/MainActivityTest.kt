package com.dhara.mvpalbums.main.view

import android.content.Context
import android.net.wifi.WifiManager
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.dhara.mvpalbums.R
import com.dhara.mvpalbums.utils.ElapsedTimeIdlingResource
import com.dhara.mvpalbums.utils.MockApiUtils
import com.google.gson.Gson
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.*
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit


@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)
    lateinit var webServer: MockWebServer

    @Before
    @Throws(Exception::class)
    fun setup() {
        setWifiEnabled(true)
        webServer = MockWebServer()
        webServer.start(8080)
    }

    @Test
    fun testLoadPhotos() {
        IdlingPolicies.setMasterPolicyTimeout(1000 * 2, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(1000 * 2, TimeUnit.MILLISECONDS)

        val idlingResource = ElapsedTimeIdlingResource(1000)
        IdlingRegistry.getInstance().register(idlingResource)

        webServer.enqueue(MockResponse().setBody(Gson().toJson(MockApiUtils.getPhotos("photos.json"))))

        IdlingRegistry.getInstance().unregister(idlingResource)
    }

    @Test
    fun testNoInternetCase() {
        IdlingPolicies.setMasterPolicyTimeout(1000 * 2, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(1000 * 2, TimeUnit.MILLISECONDS)

        val idlingResource = ElapsedTimeIdlingResource(1000)
        IdlingRegistry.getInstance().register(idlingResource)

        setWifiEnabled(false)

        IdlingRegistry.getInstance().unregister(idlingResource)

        onView(withText(R.string.error_no_network_connection))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        webServer.shutdown()
    }

    private fun setWifiEnabled(state: Boolean) {
        val wifiManager = activityRule.activity.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiManager.isWifiEnabled = state
    }
}