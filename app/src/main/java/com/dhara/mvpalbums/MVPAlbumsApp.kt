package com.dhara.mvpalbums

import android.app.Application
import com.dhara.mvpalbums.dagger2.components.DaggerAppComponent
import com.dhara.mvpalbums.dagger2.modules.AppModule

class MVPAlbumsApp : Application() {
    init {
        val appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()

        appComponent.inject(this)
    }

    companion object {
        private val INSTANCE = MVPAlbumsApp()

        val instance: MVPAlbumsApp
            get() { return INSTANCE }
    }
}