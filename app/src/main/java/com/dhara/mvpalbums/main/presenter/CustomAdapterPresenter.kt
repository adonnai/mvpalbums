package com.dhara.mvpalbums.main.presenter

import com.dhara.mvpalbums.network.entity.ImgAlbum

class CustomAdapterPresenter(val view: View): CustomAdapterContract.Presenter {
    private val imgList = mutableListOf<ImgAlbum>()

    fun onDataChange(list: List<ImgAlbum>) {
        imgList.clear()
        imgList.addAll(list)
        view.notifyAdapter()
    }

    override fun getCount() = imgList.size

    override fun getImageThumbnail(position: Int) = getItemAt(position).thumbnailUrl

    override fun getAlbumId(position: Int) = "${getItemAt(position).albumId}"

    override fun getTitle(position: Int) = getItemAt(position).title

    private infix fun getItemAt(position: Int) = imgList[position]

    interface View {
        fun notifyAdapter()
    }
}