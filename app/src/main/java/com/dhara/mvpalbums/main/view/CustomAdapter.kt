package com.dhara.mvpalbums.main.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dhara.mvpalbums.R
import com.dhara.mvpalbums.support.base.getActivityComponent
import com.dhara.mvpalbums.dagger2.modules.appmodules.adapter.CustomAdapterModule
import com.dhara.mvpalbums.main.presenter.CustomAdapterPresenter
import javax.inject.Inject

class CustomAdapter(private val listener: InteractionListener) : RecyclerView.Adapter<CustomViewHolder>(),
    CustomAdapterPresenter.View {
    @Inject
    lateinit var presenter: CustomAdapterPresenter

    init {
        getActivityComponent().plus(CustomAdapterModule(this)).inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_row, parent, false)
        return CustomViewHolder(itemView, listener)
    }

    override fun getItemCount() = presenter.getCount()

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.presenter.presenter = presenter
        holder.presenter.position = position
        holder.presenter.bind()
    }

    override fun notifyAdapter() = notifyDataSetChanged()

    interface InteractionListener {
        fun onItemClicked(position: Int)
    }
}