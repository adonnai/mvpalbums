package com.dhara.mvpalbums.main.presenter

import javax.inject.Inject

class CustomVHPresenter @Inject constructor(private val view: CustomVHPresenter.View) {
    var presenter: CustomAdapterPresenter? = null
    var position: Int? = null

    fun bind() {
        val pos = position

        pos?.let {
            if (it != -1) {
                presenter?.let { presenter ->
                    view.setImageThumbnail(presenter.getImageThumbnail(it))
                    view.setAlbumId(presenter.getAlbumId(it))
                    view.setTitle(presenter.getTitle(it))
                }
            }
        }
    }

    interface View {
        fun setImageThumbnail(imageUrl: String)

        fun setAlbumId(albumId: String)

        fun setTitle(title: String)
    }
}