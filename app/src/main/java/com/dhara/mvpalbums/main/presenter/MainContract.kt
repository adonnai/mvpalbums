package com.dhara.mvpalbums.main.presenter

import com.dhara.mvpalbums.network.entity.ImgAlbum

object MainContract {
    interface Presenter {
        fun loadPhotos()

        fun getImageList(): List<ImgAlbum>

        fun onItemClicked(position: Int)
    }
}