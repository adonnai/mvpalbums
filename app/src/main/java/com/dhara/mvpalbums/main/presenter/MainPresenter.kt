package com.dhara.mvpalbums.main.presenter

import androidx.annotation.VisibleForTesting
import com.dhara.mvpalbums.main.view.MainView
import com.dhara.mvpalbums.network.api.RestApi
import com.dhara.mvpalbums.network.entity.ImgAlbum
import com.dhara.mvpalbums.support.base.presenter.BasePresenter
import com.dhara.mvpalbums.support.coroutines.Network
import com.dhara.mvpalbums.support.exception.handler.handleException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mu.KLogging
import java.util.*
import javax.inject.Inject

class MainPresenter @Inject constructor(val view: MainView) : BasePresenter<MainView>(view), MainContract.Presenter {
    @Inject
    lateinit var restApi: RestApi

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    val imgList = ArrayList<ImgAlbum>()

    private var disposable: Disposable? = null
    var dispatcher = Dispatchers.Network

    init {
        getInjector().inject(this)
    }

    override fun loadPhotos() {
        view.showProgress()

        launch {
            withContext(dispatcher) {
                performGetPhotos()
            }
        }
    }

    override fun onItemClicked(position: Int) = view.speakText(getTitle(position))

    override fun getImageList(): List<ImgAlbum> = imgList

    override fun onViewDestroyed() {
        super.onViewDestroyed()
        if (disposable != null) {
            disposable?.dispose()
        }
    }

    private infix fun getTitle(position: Int) = imgList[position].title

    private fun performGetPhotos() {
        disposable = restApi.getPhotos()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                run {
                    if (response.isNullOrEmpty()) {
                        view.displayMessage("Image list is empty!")
                    } else {
                        imgList.clear()
                        imgList.addAll(response)
                        view.updateViews()
                    }
                    view.hideProgress()
                }
            }, { throwable ->
                run {
                    logger.error("throwable >> ${throwable.message}")
                    view.hideProgress()
                    handleException(throwable, view)
                }
            })
    }

    companion object : KLogging()
}