package com.dhara.mvpalbums.main.view

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.view.LayoutInflater
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dhara.mvpalbums.R
import com.dhara.mvpalbums.support.base.BaseActivity
import com.dhara.mvpalbums.support.base.getActivityComponent
import com.dhara.mvpalbums.dagger2.modules.appmodules.MainModule
import com.dhara.mvpalbums.main.presenter.MainPresenter
import com.dhara.mvpalbums.support.extensions.showSnackBar
import com.dhara.mvpalbums.support.widget.getDividerItemDecoration
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import javax.inject.Inject

class MainActivity : BaseActivity<MainPresenter>(), MainView, CustomAdapter.InteractionListener {
    @Inject
    lateinit var presenter: MainPresenter
    private lateinit var adapter: CustomAdapter
    private lateinit var textToSpeech: TextToSpeech
    private val pDialog: AlertDialog by lazy {
        val view = LayoutInflater.from(getContext()).inflate(R.layout.layout_progress_bar, null)
        val builder = AlertDialog.Builder(getContext()).apply {
            setCancelable(false)
            setView(view)
            setTitle(null)
        }
        builder.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getActivityComponent().plus(MainModule(this)).inject(this)

        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#1b1b1b")))

        initTTS()

        val lm = LinearLayoutManager(this).apply {
            orientation = RecyclerView.VERTICAL
        }

        adapter = CustomAdapter(this@MainActivity)

        with(rvImageList) {
            layoutManager = lm
            addItemDecoration(getDividerItemDecoration(context, lm.orientation))
            setHasFixedSize(true)
            this@MainActivity.adapter.presenter.onDataChange(presenter.getImageList())
            adapter = this@MainActivity.adapter
        }

        presenter.loadPhotos()
    }

    override fun onItemClicked(position: Int) {
        presenter.onItemClicked(position)
    }

    override fun showProgress() {
        with(pDialog) {
            setMessage("Loading...")
            show()
        }
    }

    override fun hideProgress() {
        if (pDialog.isShowing) {
            pDialog.dismiss()
        }
    }

    override fun displayMessage(message: String) = rootView.showSnackBar(message, Snackbar.LENGTH_SHORT)

    override fun updateViews() = adapter.presenter.onDataChange(presenter.getImageList())

    override fun speakText(value: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textToSpeech.speak(value, TextToSpeech.QUEUE_FLUSH, null, null)
        } else {
            textToSpeech.speak(value, TextToSpeech.QUEUE_FLUSH, null)
        }
    }

    private fun initTTS() {
        textToSpeech = TextToSpeech(getContext(), TextToSpeech.OnInitListener {
            if (it != TextToSpeech.ERROR) {
                // Setting locale to Italy since the titles are latin
                // can then add options to pick language or may be set the language during registration
                textToSpeech.language = Locale.ITALY
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (textToSpeech.isSpeaking) {
            textToSpeech.stop()
        }
        textToSpeech.shutdown()

        hideProgress()

        presenter.onViewDestroyed()
    }
}