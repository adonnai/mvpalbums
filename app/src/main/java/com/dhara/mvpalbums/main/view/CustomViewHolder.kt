package com.dhara.mvpalbums.main.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dhara.mvpalbums.R
import com.dhara.mvpalbums.support.base.getActivityComponent
import com.dhara.mvpalbums.dagger2.modules.appmodules.viewholder.CustomVHModule
import com.dhara.mvpalbums.main.presenter.CustomVHPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_row.view.*
import javax.inject.Inject

class CustomViewHolder(
    val view: View,
    listener: CustomAdapter.InteractionListener
) : RecyclerView.ViewHolder(view), CustomVHPresenter.View {
    @Inject
    lateinit var presenter: CustomVHPresenter

    init {
        getActivityComponent().plus(CustomVHModule(this)).inject(this)
        view.setOnClickListener {
            if (adapterPosition != RecyclerView.NO_POSITION) {
                listener.onItemClicked(adapterPosition)
            }
        }
    }

    override fun setImageThumbnail(imageUrl: String) {
        Picasso.get()
            .load(imageUrl)
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)
            .into(view.thumbnail)
    }

    override fun setAlbumId(albumId: String) {
        view.albumId.text = albumId
    }

    override fun setTitle(title: String) {
        view.title.text = title
    }
}