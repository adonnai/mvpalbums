package com.dhara.mvpalbums.main.view

import com.dhara.mvpalbums.support.common.view.CommonView

interface MainView: CommonView {
    fun updateViews()
}