package com.dhara.mvpalbums.main.presenter

object CustomAdapterContract {
    interface Presenter {
        fun getCount(): Int

        fun getImageThumbnail(position: Int): String

        fun getAlbumId(position: Int): String

        fun getTitle(position: Int): String
    }
}