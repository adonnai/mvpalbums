package com.dhara.mvpalbums.support.base.presenter

import com.dhara.mvpalbums.dagger2.components.injection.DaggerPresenterInjector
import com.dhara.mvpalbums.dagger2.components.injection.PresenterInjector
import com.dhara.mvpalbums.dagger2.modules.ContextModule
import com.dhara.mvpalbums.support.base.view.BaseView
import com.dhara.mvpalbums.dagger2.modules.NetModule
import com.dhara.mvpalbums.support.exception.handler.handleException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BasePresenter<out V: BaseView>(view: BaseView): CoroutineScope {
    private var rootJob: Job = Job()

    private val rootExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        run {
            handleException(throwable, view)
            rootJob = Job()
        }
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + rootJob + rootExceptionHandler

    private val injector: PresenterInjector = DaggerPresenterInjector
            .builder()
            .baseView(view)
            .contextModule(ContextModule())
            .netModule(NetModule)
            .build()

    fun getInjector() = injector

    open fun onViewDestroyed() {
        rootJob.cancel()
    }
}