package com.dhara.mvpalbums.support.extensions

import android.view.View
import com.google.android.material.snackbar.Snackbar

fun View.showSnackBar(message: String, length: Int) {
    Snackbar.make(this, message, length).show()
}