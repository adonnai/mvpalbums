package com.dhara.mvpalbums.support.common.view

import androidx.annotation.StringRes

interface MessageView {
    fun displayMessage(message: String)

    fun displayMessage(@StringRes messageRes: Int)

    fun speakText(value: String)
}