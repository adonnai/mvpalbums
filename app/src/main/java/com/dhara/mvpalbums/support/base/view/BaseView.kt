package com.dhara.mvpalbums.support.base.view

import android.content.Context
import com.dhara.mvpalbums.support.common.view.MessageView

interface BaseView: MessageView {
    fun getContext(): Context
}