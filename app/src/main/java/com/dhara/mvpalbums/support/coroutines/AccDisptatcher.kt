package com.dhara.mvpalbums.support.coroutines

import com.google.common.util.concurrent.ThreadFactoryBuilder
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asCoroutineDispatcher
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

val Dispatchers.Network: CoroutineDispatcher
    get() = networkDispatcher

private val networkDispatcher = ThreadPoolExecutor(1, 3,
        30L, TimeUnit.SECONDS,
        LinkedBlockingQueue<Runnable>(),
        ThreadFactoryBuilder()
                .setNameFormat("NetworkCoroutinesPool-%d")
                .build())
        .asCoroutineDispatcher()