package com.dhara.mvpalbums.support.exception.handler

import com.dhara.mvpalbums.R
import com.dhara.mvpalbums.support.base.view.BaseView
import com.google.gson.JsonSyntaxException
import java.net.UnknownHostException

fun handleException(throwable: Throwable, view: BaseView) {
    when (throwable) {
        is UnknownHostException -> view.displayMessage(R.string.error_no_network_connection)
        is NullPointerException -> crash(throwable)
        is JsonSyntaxException -> view.displayMessage(throwable.message.toString())
        is IllegalStateException -> crashDebugOnly(throwable)
        else -> {
            view.displayMessage(throwable.message.toString())
            crash(throwable)
        }
    }
}