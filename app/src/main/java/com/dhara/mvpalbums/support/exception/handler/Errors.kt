@file:JvmName("Errors")
package com.dhara.mvpalbums.support.exception.handler

import com.dhara.mvpalbums.BuildConfig

fun crash(e: Throwable) {
    throw RuntimeException(e)
}

fun log(e: Throwable) {
    e.printStackTrace()
}

fun crashDebugOnly(e: Throwable) {
    if (BuildConfig.DEBUG) {
        crash(e)
    } else {
        log(e)
    }
}
