package com.dhara.mvpalbums.support.widget

import android.content.Context

fun getDividerItemDecoration(context: Context, orientation: Int): DividerItemDecoration {
    return DividerItemDecoration(context, orientation, false)
}