package com.dhara.mvpalbums.support.base

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.dhara.mvpalbums.MVPAlbumsApp
import com.dhara.mvpalbums.dagger2.components.MVPAlbumComponent
import com.dhara.mvpalbums.dagger2.components.DaggerMVPAlbumComponent
import com.dhara.mvpalbums.dagger2.modules.AppModule
import com.dhara.mvpalbums.dagger2.modules.ContextModule
import com.dhara.mvpalbums.dagger2.modules.MVPAlbumAndroidModule
import com.dhara.mvpalbums.support.base.presenter.BasePresenter
import com.dhara.mvpalbums.support.base.view.BaseView
import com.dhara.mvpalbums.dagger2.modules.NetModule

fun getActivityComponent() : MVPAlbumComponent {
    return DaggerMVPAlbumComponent.builder()
            .apply {
                netModule(NetModule)
                appModule(AppModule(MVPAlbumsApp.instance))
                mVPAlbumAndroidModule(MVPAlbumAndroidModule())
                contextModule(ContextModule())
            }
            .build()
}

abstract class BaseActivity<P: BasePresenter<BaseView>>: BaseView, AppCompatActivity() {
    override fun displayMessage(messageRes: Int) {
        displayMessage(getContext().getString(messageRes))
    }

    override fun getContext(): Context = this
}