package com.dhara.mvpalbums.support.common.view

import com.dhara.mvpalbums.support.base.view.BaseView

interface CommonView: BaseView {
    fun showProgress()

    fun hideProgress()
}