package com.dhara.mvpalbums.network.entity

import com.google.gson.annotations.SerializedName

data class ImgAlbum(
        val title: String,
        val url: String,
        val albumId: Int,
        @SerializedName("thumbnailUrl")
        val thumbnailUrl: String
)