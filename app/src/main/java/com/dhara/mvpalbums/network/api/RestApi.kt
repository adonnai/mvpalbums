package com.dhara.mvpalbums.network.api

import com.dhara.mvpalbums.network.entity.ImgAlbum
import io.reactivex.Single
import retrofit2.http.GET

interface RestApi {
    @GET("photos/")
    fun getPhotos(): Single<List<ImgAlbum>>
}