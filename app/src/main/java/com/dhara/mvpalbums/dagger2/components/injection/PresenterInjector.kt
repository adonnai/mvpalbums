package com.dhara.mvpalbums.dagger2.components.injection

import com.dhara.mvpalbums.support.base.view.BaseView
import com.dhara.mvpalbums.dagger2.modules.ContextModule
import com.dhara.mvpalbums.main.presenter.MainPresenter
import com.dhara.mvpalbums.dagger2.modules.NetModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class, NetModule::class])
interface PresenterInjector {
    fun inject(mainPresenter: MainPresenter)

    @Component.Builder
    interface Builder {
        fun build(): PresenterInjector

        fun netModule(netModule: NetModule): Builder
        fun contextModule(contextModule: ContextModule): Builder

        @BindsInstance
        fun baseView(baseView: BaseView): Builder
    }
}