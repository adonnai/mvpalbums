package com.dhara.mvpalbums.dagger2.components

import android.app.Application
import com.dhara.mvpalbums.MVPAlbumsApp
import com.dhara.mvpalbums.dagger2.modules.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [AppModule::class])
interface AppComponent {
    fun inject(app: MVPAlbumsApp)

    fun application() :Application
}