package com.dhara.mvpalbums.dagger2.modules

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class AppModule (private val application : Application){
    @Provides
    fun provideApplication() : Application = application
}