package com.dhara.mvpalbums.dagger2.modules.appmodules.viewholder

import com.dhara.mvpalbums.dagger2.scopes.ActivityScope
import com.dhara.mvpalbums.main.presenter.CustomVHPresenter
import dagger.Module
import dagger.Provides

@Module
class CustomVHModule(val view: CustomVHPresenter.View) {
    @Provides
    fun providesCustomVHView() = view

    @ActivityScope
    @Provides
    fun providesCustomVHPresenter() = CustomVHPresenter(view)
}