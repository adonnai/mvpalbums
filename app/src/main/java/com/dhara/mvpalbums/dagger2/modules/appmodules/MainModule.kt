package com.dhara.mvpalbums.dagger2.modules.appmodules

import com.dhara.mvpalbums.dagger2.scopes.ActivityScope
import com.dhara.mvpalbums.main.presenter.MainPresenter
import com.dhara.mvpalbums.main.view.MainView
import dagger.Module
import dagger.Provides

@Module
class MainModule(val view: MainView) {
    @Provides
    fun providesMainView() = view

    @ActivityScope
    @Provides
    fun providesMainPresenter() = MainPresenter(view)
}