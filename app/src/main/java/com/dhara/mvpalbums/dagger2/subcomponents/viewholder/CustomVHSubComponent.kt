package com.dhara.mvpalbums.dagger2.subcomponents.viewholder

import com.dhara.mvpalbums.dagger2.modules.appmodules.viewholder.CustomVHModule
import com.dhara.mvpalbums.dagger2.scopes.ActivityScope
import com.dhara.mvpalbums.main.view.CustomViewHolder
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [CustomVHModule::class])
interface CustomVHSubComponent {
    fun inject(viewHolder: CustomViewHolder)
}