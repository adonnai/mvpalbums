package com.dhara.mvpalbums.dagger2.modules

import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class MVPAlbumAndroidModule {
    @Provides
    fun provideGson(): Gson {
        return Gson()
    }
}