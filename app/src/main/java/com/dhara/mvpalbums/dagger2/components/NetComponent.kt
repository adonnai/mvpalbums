package com.dhara.mvpalbums.dagger2.components

import com.dhara.mvpalbums.dagger2.modules.AppModule
import com.dhara.mvpalbums.network.api.RestApi
import com.dhara.mvpalbums.dagger2.modules.NetModule
import com.google.gson.Gson
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [AppModule:: class, NetModule::class])
interface NetComponent {
    // inject all data sources here
    fun getRestApi(): RestApi

    fun getGson(): Gson
}