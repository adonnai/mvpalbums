package com.dhara.mvpalbums.dagger2.subcomponents

import com.dhara.mvpalbums.dagger2.modules.appmodules.MainModule
import com.dhara.mvpalbums.dagger2.scopes.ActivityScope
import com.dhara.mvpalbums.main.view.MainActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [MainModule::class])
interface MainSubComponent {
    fun inject(activity: MainActivity)
}