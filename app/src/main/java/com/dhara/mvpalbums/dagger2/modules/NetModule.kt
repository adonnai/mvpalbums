package com.dhara.mvpalbums.dagger2.modules

import android.app.Application
import com.dhara.mvpalbums.network.api.BASE_URL
import com.dhara.mvpalbums.network.api.RestApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Singleton

@Singleton
@Module
object NetModule {
    @Provides
    @Singleton
    fun provideGson() : Gson {
        val builder = GsonBuilder().apply {
            setLenient()
        }
        return builder.create()
    }

    @Provides
    @Singleton
    fun provideHttpCache(application : Application) : Cache {
        val cacheSize : Long = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(cache: Cache) : OkHttpClient {
        val logging = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val builder = OkHttpClient.Builder().apply {
            cache(cache)
            addInterceptor(logging)
        }

        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson :Gson, okHttpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder().apply {
            addConverterFactory(GsonConverterFactory.create(gson))
            addConverterFactory(ScalarsConverterFactory.create())
            addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            client(okHttpClient)
            baseUrl(BASE_URL)
        }
                .build()
    }

    @Provides
    @Singleton
    fun getApiService(retrofit: Retrofit): RestApi {
        return retrofit.create(RestApi::class.java)
    }
}