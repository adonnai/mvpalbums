package com.dhara.mvpalbums.dagger2.components

import com.dhara.mvpalbums.dagger2.modules.MVPAlbumAndroidModule
import com.dhara.mvpalbums.dagger2.modules.AppModule
import com.dhara.mvpalbums.dagger2.modules.ContextModule
import com.dhara.mvpalbums.dagger2.modules.NetModule
import com.dhara.mvpalbums.dagger2.modules.appmodules.MainModule
import com.dhara.mvpalbums.dagger2.modules.appmodules.adapter.CustomAdapterModule
import com.dhara.mvpalbums.dagger2.modules.appmodules.viewholder.CustomVHModule
import com.dhara.mvpalbums.dagger2.subcomponents.MainSubComponent
import com.dhara.mvpalbums.dagger2.subcomponents.adapter.CustomAdapterSubComponent
import com.dhara.mvpalbums.dagger2.subcomponents.viewholder.CustomVHSubComponent
import dagger.Component

@Component(modules = [MVPAlbumAndroidModule::class, AppModule::class, NetModule::class, ContextModule::class])
interface MVPAlbumComponent {
    fun plus(module: MainModule): MainSubComponent

    fun plus(module: CustomAdapterModule): CustomAdapterSubComponent

    fun plus(module: CustomVHModule): CustomVHSubComponent
}