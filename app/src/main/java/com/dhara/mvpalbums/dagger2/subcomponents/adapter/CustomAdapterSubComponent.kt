package com.dhara.mvpalbums.dagger2.subcomponents.adapter

import com.dhara.mvpalbums.dagger2.modules.appmodules.adapter.CustomAdapterModule
import com.dhara.mvpalbums.dagger2.scopes.ActivityScope
import com.dhara.mvpalbums.main.view.CustomAdapter
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [CustomAdapterModule::class])
interface CustomAdapterSubComponent {
    fun inject(adapter: CustomAdapter)
}