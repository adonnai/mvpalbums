package com.dhara.mvpalbums.dagger2.modules

import android.app.Application
import android.content.Context
import com.dhara.mvpalbums.support.base.view.BaseView
import dagger.Module
import dagger.Provides

@Module
class ContextModule {
    @Provides
    fun provideContext(baseView: BaseView) = baseView.getContext()

    @Provides
    fun provideApplication(context: Context): Application = context.applicationContext as Application
}