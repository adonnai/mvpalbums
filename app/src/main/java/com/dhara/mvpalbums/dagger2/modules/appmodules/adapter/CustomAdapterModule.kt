package com.dhara.mvpalbums.dagger2.modules.appmodules.adapter

import com.dhara.mvpalbums.dagger2.scopes.ActivityScope
import com.dhara.mvpalbums.main.presenter.CustomAdapterPresenter
import dagger.Module
import dagger.Provides

@Module
class CustomAdapterModule(val view: CustomAdapterPresenter.View) {
    @Provides
    fun providesCustomAdapterView() = view

    @ActivityScope
    @Provides
    fun providesCustomAdapterPresenter() = CustomAdapterPresenter(view)
}