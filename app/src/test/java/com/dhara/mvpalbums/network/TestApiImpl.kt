package com.dhara.mvpalbums.network

import com.dhara.mvpalbums.utils.ApiUtils
import com.dhara.mvpalbums.utils.PHOTOS_MOCK_PATH
import com.dhara.mvpalbums.network.api.RestApi
import com.dhara.mvpalbums.network.entity.ImgAlbum
import io.reactivex.Single

class TestApiImpl: RestApi {
    override fun getPhotos(): Single<List<ImgAlbum>> =
            Single.just<List<ImgAlbum>>(ApiUtils.getPhotos(PHOTOS_MOCK_PATH))
}