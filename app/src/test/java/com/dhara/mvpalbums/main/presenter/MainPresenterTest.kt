package com.dhara.mvpalbums.main.presenter

import android.os.Handler
import android.os.Looper
import androidx.annotation.NonNull
import com.dhara.mvpalbums.MVPAlbumsBaseTest
import com.dhara.mvpalbums.R
import com.dhara.mvpalbums.main.view.MainView
import com.dhara.mvpalbums.network.entity.ImgAlbum
import com.dhara.mvpalbums.utils.ApiUtils
import com.dhara.mvpalbums.utils.PHOTOS_MOCK_PATH
import com.google.gson.JsonSyntaxException
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.coroutines.*
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.*
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.doAnswer
import org.mockito.MockitoAnnotations
import org.mockito.stubbing.Answer
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PowerMockIgnore
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import java.net.UnknownHostException
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.assertTrue

fun setUpRxSchedulers() {
    val immediate = object : Scheduler() {
        override fun scheduleDirect(@NonNull run: Runnable, delay: Long, @NonNull unit: TimeUnit): Disposable {
            // this prevents StackOverflowErrors when scheduling with a delay
            return super.scheduleDirect(run, 0, unit)
        }

        override fun createWorker(): Scheduler.Worker {
            return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
        }
    }

    RxJavaPlugins.setInitIoSchedulerHandler { _ -> immediate }
    RxJavaPlugins.setInitComputationSchedulerHandler { _ -> immediate }
    RxJavaPlugins.setInitNewThreadSchedulerHandler { _ -> immediate }
    RxJavaPlugins.setInitSingleSchedulerHandler { _ -> immediate }
    RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> immediate }
}

@RunWith(PowerMockRunner::class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Looper::class, Handler::class)
@ExperimentalCoroutinesApi
class MainPresenterTest : MVPAlbumsBaseTest() {
    @Mock
    lateinit var view: MainView
    lateinit var presenter: MainPresenter

    @Before
    @Throws(Exception::class)
    override fun setUp() {
        MockitoAnnotations.initMocks(this)
        PowerMockito.mockStatic(Looper::class.java, Handler::class.java)

        mockHandler()
        setUpRxSchedulers()

        Dispatchers.setMain(Dispatchers.Unconfined)

        Mockito.`when`(app.applicationContext).thenReturn(app)
        Mockito.`when`(contextModule.provideApplication(app)).thenReturn(app)
        Mockito.`when`(view.getContext()).thenReturn(app)
        Mockito.`when`(contextModule.provideContext(view)).thenReturn(app)

        presenter = MainPresenter(view)
        presenter.dispatcher = Dispatchers.Unconfined
        presenter.restApi = restApi
    }

    @Test
    fun testLoadPhotos() {
        PHOTOS_MOCK_PATH = "photos.json"
        val response: List<ImgAlbum> = ApiUtils.getPhotos(PHOTOS_MOCK_PATH)
        val singleResponse = Single.just<List<ImgAlbum>>(response)
        Mockito.`when`(restApi.getPhotos()).thenReturn(singleResponse)

        runBlocking {
            presenter.loadPhotos()
            verify(view).getContext()
            verify(view).showProgress()
            assertEquals(presenter.imgList.size, response.size, "same size")
            verify(view).updateViews()
            verify(view).hideProgress()
        }
    }

    @Test
    fun testLoadPhotosEmptyResponse() {
        PHOTOS_MOCK_PATH = "empty.json"
        val response: List<ImgAlbum> = ApiUtils.getPhotos(PHOTOS_MOCK_PATH)
        val singleResponse = Single.just<List<ImgAlbum>>(response)
        Mockito.`when`(restApi.getPhotos()).thenReturn(singleResponse)

        runBlocking {
            presenter.loadPhotos()
            verify(view).getContext()
            verify(view).showProgress()

            assertTrue("the list is empty") {
                presenter.imgList.isNullOrEmpty()
            }

            assertEquals(response.isNullOrEmpty(), presenter.imgList.isNullOrEmpty())
            verify(view).displayMessage("Image list is empty!")
            verify(view).hideProgress()
        }
    }

    @Test
    fun testLoadPhotosError() {
        Mockito.`when`(restApi.getPhotos()).thenReturn(Single.error(JsonSyntaxException("json error")))

        runBlocking {
            presenter.loadPhotos()
            verify(view).getContext()
            verify(view).showProgress()
            verify(view).hideProgress()
            verify(view).displayMessage("json error")
        }
    }

    @Test
    fun testLoadPhotosNoInternet() {
        Mockito.`when`(restApi.getPhotos()).thenReturn(Single.error(UnknownHostException()))

        runBlocking {
            presenter.loadPhotos()
            verify(view).getContext()
            verify(view).showProgress()
            verify(view).hideProgress()
            verify(view).displayMessage(eq(R.string.error_no_network_connection))
        }
    }

    @After
    fun after() {
        verify(view).getContext()
        verifyNoMoreInteractions(view)
    }

    private val mainThread = Executors.newSingleThreadScheduledExecutor()

    private fun mockHandler() {
        val mockMainThreadLooper = Mockito.mock(Looper::class.java)
        Mockito.`when`(Looper.getMainLooper()).thenReturn(mockMainThreadLooper)
        val mockMainThreadHandler = Mockito.mock(Handler::class.java)
        val handlerPostAnswer: Answer<Boolean> = Answer { invocation ->
            val runnable = invocation.getArgument<Runnable>(0)
            var delay: Long? = 0L
            if (invocation.arguments.size > 1) {
                delay = invocation.getArgument<Long>(1)
            }
            if (runnable != null) {
                mainThread.schedule(runnable, delay ?: 0, TimeUnit.MILLISECONDS)
            }
            true
        }

        doAnswer(handlerPostAnswer).`when`(mockMainThreadHandler).post(any(Runnable::class.java))
        doAnswer(handlerPostAnswer).`when`(mockMainThreadHandler).postDelayed(any(Runnable::class.java), anyLong())
        PowerMockito.whenNew(Handler::class.java).withArguments(mockMainThreadLooper).thenReturn(mockMainThreadHandler)
    }
}