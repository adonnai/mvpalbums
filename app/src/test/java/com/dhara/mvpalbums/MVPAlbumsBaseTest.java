package com.dhara.mvpalbums;

import android.content.Context;

import androidx.annotation.CallSuper;
import com.dhara.mvpalbums.dagger2.components.AppComponent;
import com.dhara.mvpalbums.dagger2.components.DaggerAppComponent;
import com.dhara.mvpalbums.dagger2.components.DaggerMVPAlbumComponent;
import com.dhara.mvpalbums.dagger2.modules.AppModule;
import com.dhara.mvpalbums.dagger2.modules.ContextModule;
import com.dhara.mvpalbums.dagger2.modules.MVPAlbumAndroidModule;
import com.dhara.mvpalbums.dagger2.modules.NetModule;
import com.dhara.mvpalbums.network.TestApiImpl;
import com.dhara.mvpalbums.network.api.RestApi;
import com.dhara.mvpalbums.support.base.view.BaseView;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ContextModule.class, NetModule.class})
public class MVPAlbumsBaseTest {
    protected @Mock
    ContextModule contextModule;
    protected @Mock
    MVPAlbumsApp app;
    protected @Mock
    RestApi restApi;
    @Mock
    AppModule appModule;
    @Mock
    NetModule netModule;
    @Mock
    MVPAlbumAndroidModule mvpAlbumAndroidModule;
    @Mock
    Retrofit retrofit;
    @Mock
    OkHttpClient okHttpClient;
    @Mock
    Cache cache;
    @Mock
    BaseView baseView;
    @Mock
    protected Context context;
    @Mock
    File cacheDir;
    private AppComponent appComponent;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(ContextModule.class, NetModule.class);

        restApi = mock(TestApiImpl.class);

        appComponent = DaggerAppComponent.builder().appModule(appModule).build();

        when(context.getApplicationContext()).thenReturn(app);
        when(app.getCacheDir()).thenReturn(cacheDir);
        when(netModule.provideGson()).thenReturn(new Gson());
        when(netModule.provideHttpCache(app)).thenReturn(cache);
        when(netModule.provideOkhttpClient(cache)).thenReturn(okHttpClient);
        when(netModule.provideRetrofit(any(Gson.class), any(OkHttpClient.class))).thenReturn(retrofit);
        when(netModule.getApiService(retrofit)).thenReturn(restApi);
        when(baseView.getContext()).thenReturn(context);
        when(contextModule.provideContext(baseView)).thenReturn(context);
        when(contextModule.provideApplication(context)).thenReturn(app);

        DaggerMVPAlbumComponent.builder()
                .appModule(appModule)
                .mVPAlbumAndroidModule(mvpAlbumAndroidModule)
                .netModule(netModule)
                .contextModule(contextModule)
                .build();
    }

    @Test
    public void emptyTest() {
        //to compile with Runner
    }
}








